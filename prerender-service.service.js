(function () {

    'use strict';

    function preRenderService ($rootScope, $state) {
        return {
            state301: function (location, redirectLocationIdentifier, redirectLocationParams) {
                /** 301 Prerender - Sets the Pre Render Status code **/
                $rootScope.preRenderStatus = 301;
                $rootScope.preRender301Status = true;
                $rootScope.preRenderHeader = 'Location: ' + location;

                // Don't execute redirect for Prerender
                if (navigator.userAgent.toLowerCase().indexOf('prerender') == -1) {
                    $state.transitionTo(redirectLocationIdentifier, redirectLocationParams);
                }
            }
        }
    }

    preRenderService.$inject = ['$rootScope', '$state'];

    angular
        .module('bravoureAngularApp')
        .factory ('preRenderService', preRenderService);

})();
