# Bravoure - Service Prerender

## Use

This Component sets the different pages states for prerender.

### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-service-prerender": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
